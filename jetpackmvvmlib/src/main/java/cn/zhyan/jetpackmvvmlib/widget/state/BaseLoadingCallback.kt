package cn.zhyan.jetpackmvvmlib.widget.state

import android.content.Context
import android.view.View
import cn.zhyan.jetpackmvvmlib.R
import cn.zhyan.jetpackmvvmlib.loadsir.callback.Callback

class BaseLoadingCallback: Callback() {

    override fun onCreateView(): Int {
        return R.layout.layout_loading
    }

    /**
     * 是否是 点击不可重试
     */
    override fun onReloadEvent(context: Context?, view: View?): Boolean {
        return true
    }
}