package cn.zhyan.jetpackmvvmlib.widget.state

import cn.zhyan.jetpackmvvmlib.R
import cn.zhyan.jetpackmvvmlib.loadsir.callback.Callback

class BaseErrorCallback : Callback() {

    override fun onCreateView(): Int {
        return R.layout.layout_error
    }

}