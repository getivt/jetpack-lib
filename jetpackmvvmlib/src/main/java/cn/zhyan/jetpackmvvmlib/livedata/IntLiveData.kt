package cn.zhyan.jetpackmvvmlib.livedata

import com.kunminx.architecture.ui.callback.UnPeekLiveData

/**
 * 自定义的Int类型 MutableLiveData 提供了默认值，避免取值的时候还要判空
 */
class IntLiveData : UnPeekLiveData<Int>() {

    override fun getValue(): Int {
        return super.getValue() ?: 0
    }
}