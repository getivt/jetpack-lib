package cn.zhyan.jetpackmvvmlib.livedata

import com.kunminx.architecture.ui.callback.UnPeekLiveData


/**
 * 自定义的Float类型 MutableLiveData 提供了默认值，避免取值的时候还要判空
 */
class FloatLiveData(value: Float = 0f) : UnPeekLiveData<Float>(value) {
    override fun getValue(): Float {
        return super.getValue()!!
    }
}