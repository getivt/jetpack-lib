package cn.zhyan.jetpackmvvmlib.livedata

import com.kunminx.architecture.ui.callback.UnPeekLiveData


/**
 * 自定义的Byte类型 MutableLiveData 提供了默认值，避免取值的时候还要判空
 */
class ByteLiveData : UnPeekLiveData<Byte>() {
    override fun getValue(): Byte {
        return super.getValue() ?: 0
    }
}