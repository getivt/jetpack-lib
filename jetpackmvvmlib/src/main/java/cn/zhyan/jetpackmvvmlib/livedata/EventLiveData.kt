package cn.zhyan.jetpackmvvmlib.livedata

import com.kunminx.architecture.ui.callback.UnPeekLiveData

/**
 * 最新版的发送消息LiveData使用 https://github.com/KunMinX/UnPeek-LiveData 的最新版，
 * 解决 “数据倒灌” 问题
 */
class EventLiveData<T> : UnPeekLiveData<T>()