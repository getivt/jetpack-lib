package cn.zhyan.jetpackmvvmlib.livedata

import com.kunminx.architecture.ui.callback.UnPeekLiveData

/**
 * 自定义的String类型 MutableLiveData 提供了默认值，避免取值的时候还要判空
 */
class StringLiveData : UnPeekLiveData<String>() {

    override fun getValue(): String {
        return super.getValue() ?: ""
    }

}