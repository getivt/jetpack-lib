package cn.zhyan.jetpackmvvmlib.base.activity

import androidx.appcompat.app.AppCompatActivity

abstract class BaseInitActivity : AppCompatActivity() {

    abstract val layoutId: Int

}