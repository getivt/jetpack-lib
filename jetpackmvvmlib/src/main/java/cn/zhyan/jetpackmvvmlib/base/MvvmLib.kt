package cn.zhyan.jetpackmvvmlib.base

import android.app.Application
import android.view.Gravity
import com.hjq.toast.ToastUtils
import cn.zhyan.jetpackmvvmlib.ext.util.dp
import cn.zhyan.jetpackmvvmlib.loadsir.callback.SuccessCallback
import cn.zhyan.jetpackmvvmlib.loadsir.core.LoadSir
import cn.zhyan.jetpackmvvmlib.util.KtxActivityLifecycleCallbacks
import cn.zhyan.jetpackmvvmlib.util.mvvmLibLog
import cn.zhyan.jetpackmvvmlib.widget.state.BaseEmptyCallback
import cn.zhyan.jetpackmvvmlib.widget.state.BaseErrorCallback
import cn.zhyan.jetpackmvvmlib.widget.state.BaseLoadingCallback

/**
 * 全局上下文，可直接拿
 */
val appContext: Application by lazy { MvvmLib.app }

object MvvmLib {

    lateinit var app: Application

    /**
     * 框架初始化
     * @param application Application 全局上下文
     * @param debug Boolean  true为debug模式，会打印Log日志 false 关闭Log日志
     */
    fun init(application: Application, debug: Boolean) {
        app = application
        mvvmLibLog = debug
        //注册全局 activity生命周期监听
        application.registerActivityLifecycleCallbacks(KtxActivityLifecycleCallbacks())
        LoadSir.beginBuilder()
            .setErrorCallBack(BaseErrorCallback())
            .setEmptyCallBack(BaseEmptyCallback())
            .setLoadingCallBack(BaseLoadingCallback())
            .setDefaultCallback(SuccessCallback::class.java)
            .commit()
        ToastUtils.init(app)
        ToastUtils.setGravity(Gravity.BOTTOM, 0, 100.dp)
    }
}