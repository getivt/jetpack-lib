package cn.zhyan.jetpackmvvmlib.base.fragment

import androidx.fragment.app.Fragment

abstract class BaseInitFragment : Fragment() {

    abstract val layoutId: Int

}