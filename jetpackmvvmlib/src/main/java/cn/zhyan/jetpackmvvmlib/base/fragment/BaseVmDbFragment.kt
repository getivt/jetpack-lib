package cn.zhyan.jetpackmvvmlib.base.fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import cn.zhyan.jetpackmvvmlib.base.BaseIView
import cn.zhyan.jetpackmvvmlib.base.viewmodel.BaseViewModel
import cn.zhyan.jetpackmvvmlib.ext.view.inflateBinding

/**
 * description　: ViewModelFragment基类，自动把ViewModel注入Fragment和Databind注入进来了
 * 需要使用Databind的清继承它
 */
abstract class BaseVmDbFragment<VM : BaseViewModel,DB: ViewDataBinding> : BaseVmFragment<VM>(),BaseIView {

    //使用了 DataBinding 就不需要 layoutId了，因为 会从 VB 泛型 找到相关的view
    override val layoutId: Int = 0

    private var _binding: DB? = null
    val mBind: DB get() = _binding!!

    /**
     * 创建 DataBinding
     */
    override fun initViewDataBind(inflater: LayoutInflater, container: ViewGroup?): View? {
        _binding = inflateBinding(inflater, container, false)
        _binding?.lifecycleOwner = viewLifecycleOwner
        return mBind.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}