package cn.zhyan.jetpackmvvmlib.base.activity

import android.view.View
import androidx.viewbinding.ViewBinding
import cn.zhyan.jetpackmvvmlib.base.BaseIView
import cn.zhyan.jetpackmvvmlib.base.viewmodel.BaseViewModel
import cn.zhyan.jetpackmvvmlib.ext.view.inflateBinding
import com.noober.background.BackgroundLibrary

/**
 * description　: 包含 ViewModel 和 ViewBinding ViewModelActivity基类，把ViewModel 和 ViewBinding 注入进来了
 * 需要使用 ViewBinding 的清继承它
 */
abstract class BaseVmVbActivity<VM : BaseViewModel,VB: ViewBinding> : BaseVmActivity<VM>(),BaseIView {

    //使用了 ViewBinding 就不需要 layoutId了，因为 会从 VB 泛型 找到相关的view
    override val layoutId: Int = 0
    lateinit var mBind: VB

    override fun initViewDataBind(): View? {
        //利用反射 根据泛型得到 ViewBinding
        mBind = inflateBinding()
        BackgroundLibrary.inject(this)
        return mBind.root
    }
}