package cn.zhyan.jetpackmvvmlib.base.activity

import android.view.View
import androidx.databinding.ViewDataBinding
import cn.zhyan.jetpackmvvmlib.base.BaseIView
import cn.zhyan.jetpackmvvmlib.base.viewmodel.BaseViewModel
import cn.zhyan.jetpackmvvmlib.ext.view.inflateBinding
import com.noober.background.BackgroundLibrary

/**
 * description　: 包含ViewModel 和Databind ViewModelActivity基类，把ViewModel 和Databind注入进来了
 * 需要使用Databind的清继承它
 */
abstract class BaseVmDbActivity<VM : BaseViewModel,DB: ViewDataBinding> : BaseVmActivity<VM>(),BaseIView {

    //使用了DataBinding 就不需要 layoutId了，因为 会从DB泛型 找到相关的view
    override val layoutId: Int = 0

    lateinit var mBind: DB

    /**
     * 创建DataBinding
     */
    override fun initViewDataBind(): View? {
        //利用反射 根据泛型得到 ViewDataBinding
        mBind = inflateBinding()
        BackgroundLibrary.inject(this)
        mBind.lifecycleOwner = this
        return mBind.root
    }

}